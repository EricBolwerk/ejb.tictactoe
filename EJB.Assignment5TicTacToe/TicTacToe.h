#pragma once
#include <iostream>

using namespace std;

class TicTacToe
{
private:
	int m_numTurns = 0;
	char m_playerTurn;
	char m_winner;
	char m_board[10] = { "123456789" };
public:
	void DisplayBoard()
	{
		cout << "\t" << m_board[0] << ",\t" << m_board[1] << ",\t" << m_board[2] << "\n";
		cout << "\t" << m_board[3] << ",\t" << m_board[4] << ",\t" << m_board[5] << "\n";
		cout << "\t" << m_board[6] << ",\t" << m_board[7] << ",\t" << m_board[8] << "\n";
	}

	bool IsOver()
	{
		for (int i = 0; i < 3; i++)
		{
			if (m_board[i] == m_board[i + 3] && m_board[i + 3] == m_board[i + 6])
			{
				if (m_board[i] == 'x' || m_board[i] == 'o')
				{
					m_winner = m_board[i];
					return(true);
				}
			}
		}

		for (int i = 0; i < 9; i = i + 3)
		{
			if (m_board[i] == m_board[i + 1] && m_board[i + 1] == m_board[i + 2])
			{
				if (m_board[i] == 'x' || m_board[i] == 'o')
				{
					m_winner = m_board[i];
					return(true);
				}
			}
		}

		if (m_board[2] == m_board[4] && m_board[4] == m_board[6])
		{
			if (m_board[2] == 'x' || m_board[2] == 'o')
			{
				m_winner = m_board[2];
				return(true);
			}
		}

		if (m_board[0] == m_board[4] && m_board[4] == m_board[8])
		{
			if (m_board[0] == 'x' || m_board[0] == 'o')
			{
				m_winner = m_board[0];
				return(true);
			}
		}

		if (m_numTurns < 9) return(false);

		return(true);
	}

	char GetPlayerTurn()
	{
		if (m_numTurns % 2 == 1) m_playerTurn = 'o';
		else m_playerTurn = 'x';

		return(m_playerTurn);
	}

	bool IsValidMove(int position)
	{
		position--;
		if (m_board[position] == 'o' || m_board[position] == 'x') return(false);

		else return(true);
	}

	void Move(int position)
	{
		position--;
		m_numTurns++;
		m_board[position] = m_playerTurn;
	}

	void DisplayResult()
	{
		if (m_winner == 'o' || m_winner == 'x')
		{
			cout << "The winner is player " << m_winner << ".\n";
		}

		else
		{
			cout << "It is a tie.\n";
		}
	}
};